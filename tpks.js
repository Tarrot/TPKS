// ==UserScript==
// @name         Tarrot's PokeClicker Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Made with love for you to beat the shit out of those pokemon <3
// @author       Tarrot
// @match        https://www.pokeclicker.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=pokeclicker.com
// @grant        none
// ==/UserScript==

//
// Edit these :)
//
let tpks_wallet_multi = 10; // When adding to the wallet, 1,000 is added.  This is the multiplier,  gainMoney(1000 * tpks_wallet_multi);. ( integer )
let tpks_battle_tick = 10; // How many miliseconds do we invoke the clicking function ( tpks_clk() ). ( integer )
let tpks_endless_pkball = true; // Gets the current pokeball value then replaces the pokeball value when it changes to be lower. ( true / false )

let tpks_gym_endless_time = true; // Constantly reset the gym timer. ( true / false )
let tpks_gym_insta_win = false; // Instantly win gym battles. ( true / false )

let tpks_dungeon_endless_time = true; // Constantly reset the dungeon timer. ( true / false )
let tpks_dungeon_insta_win = false; // Instantly win dungons. ( true / false )
let tpks_dungeon_show_chest_tiles = true; // Show on load all the chests tiles. ( true / false )
let tpks_dungeon_show_all_tiles = false; // Show all tiles on load. ( true / false )

let tpks_tmpbattle_endless_time = true; // Constantly reset the temp battle timer. ( true / false )
let tpks_tmpbattle_insta_win = false; // Instantly win temp battles. ( true / false )

let tpks_underground_auto_bomb = true; // Start bombing the mine upon opening. ( true / false )
let tpks_underground_infinte_energy = true; // Underground has endless energy. ( true / false )

//
// You probibly shouldnt edit below this
//
let tpks_dbg = true;  // debug
let tpks_omg = -1;
let tpks_uwu = -1;
let tpks_sb = 0;
let tpks_dst = false;
let tpks_tps = 59990;


// Clicker
function tpks_clk()
{
    switch(App.game.gameState)
    {
        case 2: // Route battle
            Battle.clickAttack();
            break;
        case 3: // Gym battle
            GymBattle.clickAttack();
            break;
        case 4: // Dungeon battle ( +chest +exit )
            DungeonRunner.handleClick();
            break;
        case 9: // Tempt battle ( Trainer, rival, etc )
            TemporaryBattleBattle.clickAttack();
            break;
    }
}

// Toggle clicker
function tpks_otc()
{
    if(tpks_omg === -1) { console.log("[TPKS] Toggle on."); tpks_omg = setInterval(tpks_clk, 1); }
    else { console.log("[TPKS] Toggle off."); clearInterval(tpks_omg); tpks_omg = -1; }
}

// Wallet functions
function tpks_am() { App.game.wallet.gainMoney(1000 * tpks_wallet_multi); }
function tpks_abt() { App.game.wallet.gainDungeonTokens(1000 * tpks_wallet_multi); }
function tpks_aqp() { App.game.wallet.gainQuestPoints(1000 * tpks_wallet_multi); }
function tpks_ad() { App.game.wallet.gainDiamonds(100 * tpks_wallet_multi); }

// Replace Start Menu Bug Report button.
// TODO fix this
function tpks_esm()
{
    setTimeout(() => {
        console.log("[TPKS] Trying to replace \"Report a bug\".");
        let _start_menu = document.getElementsByClassName("dropdown-menu dropdown-menu-right show")[0];
        let tpks_btn = _start_menu[_start_menu.length - 1].children[0];
        tpks_btn.innerHTML = "TPKS Settings";
        tpks_btn.href = "#";
        tpks_btn.addEventListener("click", () => { console.log("[TPKS] TODO: this.")});
    }, 10);
}

// Main game tick function
function tpks_mgt()
{
    // Set pokeballs
    if(App.game.pokeballs.pokeballs[0].quantity() > tpks_sb) { tpks_sb = App.game.pokeballs.pokeballs[0].quantity(); console.log("[TPKS] Updated pokeballs."); }
    else if(App.game.pokeballs.pokeballs[0].quantity() < tpks_sb) { App.game.pokeballs.pokeballs[0].quantity(tpks_sb); }

    switch(App.game.gameState)
    {
        case 3: // Gym Battles
            if(tpks_gym_insta_win) { GymRunner.gymWon(); } // Insta win
            else
            {
                // Endless time
                if(tpks_gym_endless_time) { GymRunner.timeLeft(tpks_tps); }
            }
            break;
        case 4: // Dungeon Cheats
            if(tpks_dungeon_insta_win) { DungeonRunner.dungeonWon(); } // Insta win
            else
            {
                // Endless time
                if(tpks_dungeon_endless_time) { DungeonRunner.timeLeft(tpks_tps); }
                // Show all chest tiles
                if(tpks_dungeon_show_chest_tiles) { DungeonRunner.map.showChestTiles(); }
                // Show all tiles
                else if(tpks_dungeon_show_all_tiles) { DungeonRunner.map.showAllTiles(); }

            }
            break;
        case 9: // Temp battle
            if(tpks_tmpbattle_insta_win) { TemporaryBattleRunner.battleWon(); } // Insta win
            else
            {
                // Endless time
                if(tpks_tmpbattle_endless_time) { TemporaryBattleRunner.timeLeft(tpks_tps); }
            }
            break;
    }

    // Reset dungeon things
    if(App.game.gameState != 4) { tpks_dst = false; }

    // Underground
    if(document.getElementById("mineModal").className.match("show"))
    {
        console.log("[TPKS] Opened underground.");

        // Infinate energy
        if(App.game.underground.energy < App.game.underground.getMaxEnergy() && tpks_underground_infinte_energy) { App.game.underground.energy = App.game.underground.getMaxEnergy(); }
        // Auto bombing
        if(tpks_underground_auto_bomb) { Mine.bomb(); }
    }

    // Farm
    if(document.getElementById("farmModal").className.match("show"))
    {
        console.log("[TPKS] Opened farm.");
        // TODO add stuff here
    }
}

// We do this to try to hook all the assets we need when a save is loaded.
function tpks_fgt()
{
    if(document.getElementById("gameTitle") != null)
    {
        try
        {
            tpks_sb = App.game.pokeballs.pokeballs[0].quantity();  // This normally thows erros until loaded into a game.
            document.getElementById("startMenu").addEventListener("click", tpks_esm);
            document.getElementById("gameTitle").addEventListener("click", tpks_otc);
            document.getElementById("animateCurrency-money").addEventListener("click", tpks_am);
            document.getElementById("animateCurrency-dungeonToken").addEventListener("click", tpks_abt);
            document.getElementById("animateCurrency-questPoint").addEventListener("click", tpks_aqp);
            document.getElementById("diamondCounter").addEventListener("click", tpks_ad);
            // TODO hook FarmToken

            setInterval(tpks_mgt, 100);

            console.log("[TPKS] Found it.");
            clearInterval(tpks_uwu);
        } catch(e) { if(tpks_dbg) { console.error(e); } }
    }
}

(function() {
    'use strict';
    tpks_uwu = setInterval(tpks_fgt, 50);
    // Your code here...
})();
